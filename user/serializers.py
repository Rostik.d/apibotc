from rest_framework import serializers

from .models import UserBot


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserBot
        fields = '__all__'
