from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from rest_framework.authtoken.models import Token

from utils.models import UUIDModel, TimeStamp


class CustomUserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, password, **extra_fields):
        user = self.model(**extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, password, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_active', True)
        return self._create_user(password, **extra_fields)

    def create_superuser(self, password, **extra_fields):
        extra_fields.setdefault('is_active', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)
        if extra_fields.get('is_superuser') and extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_superuser=True and is_staff=True.')
        return self._create_user(password, **extra_fields)


class Group(models.Model):
    name = models.CharField('Group name', max_length=125, unique=True)

    objects = models.Manager()

    class Meta:
        verbose_name = 'Група'
        verbose_name_plural = 'Групи'

    def str(self):
        return self.name


AUTH_PROVIDERS = {'facebook': 'facebook', 'google': 'google',
                  'email': 'email'}


class User(UUIDModel, AbstractBaseUser, PermissionsMixin, TimeStamp):
    class Gender(models.TextChoices):
        MAN = 'Man'
        WOMAN = 'Woman'

    email = models.CharField(max_length=255, unique=True, default='')
    username = models.CharField(max_length=255, unique=True)

    group_id = models.ForeignKey(Group, on_delete=models.DO_NOTHING, related_name='group', blank=True, null=True)

    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    auth_provider = models.CharField(
        max_length=255, blank=False, null=False, default=AUTH_PROVIDERS.get('email')
    )

    last_login = None
    groups = None
    user_permissions = None

    USERNAME_FIELD = 'email'

    objects = CustomUserManager()

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        return self

    def __str__(self):
        return f'{self.email} - {self.created_ad}'

    class Meta:
        verbose_name = "Користувач"
        verbose_name_plural = "Користувачі"


class UserBot(UUIDModel, TimeStamp):
    username = models.CharField(max_length=255, unique=True)
    tg_user_id = models.CharField(max_length=255, verbose_name='Telegram User id')
    tg_login = models.CharField(max_length=100, verbose_name='Telegram login')
    tg_language_code = models.CharField(max_length=10, verbose_name='Telegram Language code')
    tg_is_bot = models.BooleanField(default=False, verbose_name='Is bot')

    def __str__(self):
        return f'{self.tg_login} - {self.tg_user_id} - {self.tg_language_code} - {self.created_ad}'

    class Meta:
        verbose_name = "Користувач"
        verbose_name_plural = "Користувачі"

