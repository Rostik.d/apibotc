from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ModelViewSet

from .models import UserBot
from .serializers import UserSerializer


class UserAPIView(ModelViewSet):
    queryset = UserBot.objects.all()
    permission_classes = [AllowAny]

    def get_serializer_class(self):
        if self.action == "create":
            return UserSerializer
