# from rest_framework_nested import routers
#
# from .views import UserAPIView
#
# router = routers.SimpleRouter()
# router.register("", UserAPIView, basename="user_view")
from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import UserAPIView

router = DefaultRouter()
router.register('user', UserAPIView)

urlpatterns = [
    path('', include(router.urls)),
]
